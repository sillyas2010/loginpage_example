<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Model for user's auth tokens.
 *
 * @property integer $user_id
 * @property string $token
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class AuthToken extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'token'], 'required'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'token' => 'Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Generate and save auth token for user.
     *
     * @param \app\models\User $user
     *
     * @return \app\models\AuthToken
     */
    public static function generate(User $user)
    {
        $token = new AuthToken([
            'user_id' => $user->id,
            'token' => Yii::$app->security->generateRandomString()
        ]);
        $token->save();
        return $token;
    }
}

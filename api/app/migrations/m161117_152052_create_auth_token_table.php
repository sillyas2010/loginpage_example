<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m161117_152052_create_auth_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('auth_token', [
            'user_id' => $this->integer(),
            'token' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addPrimaryKey('auth_token', 'auth_token', 'token');
        $this->createIndex('idx-auth_token-user_id', '{{%auth_token}}', 'user_id');
        $this->addForeignKey('auth_token_to_user', 'auth_token', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('auth_token');
    }
}

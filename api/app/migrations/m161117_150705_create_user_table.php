<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m161117_150705_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id'            => $this->primaryKey(),
            'username'      => $this->string()->notNull(),
            'name'          => $this->string()->notNull(),
            'auth_key'      => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'email'         => $this->string()->notNull(),
            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-user-username', '{{%user}}', 'username');

        /** Create test user */
        $user = new \app\models\User([
            'username' => 'admin',
            'name'     => 'admin',
            'email'    => 'admin@admin.ru',
        ]);
        $user->setPassword('admin');
        $user->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

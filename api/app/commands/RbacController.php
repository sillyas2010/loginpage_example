<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        `yii migrate --migrationPath=@yii/rbac/migrations --interactive=0`;
        echo '-- RBAC migrated';
        `yii migrate --interactive=0`;
        echo '-- User migrated';
        $admin = new User([
            'username' => 'admin',
        ]);
        $admin->setPassword('admin')
            ->save();
        echo '-- User created';

        /*$auth = \Yii::$app->authManager;
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);*/
    }
}
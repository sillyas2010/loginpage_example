<?php

namespace app\controllers;

use app\models\AuthToken;
use app\models\User;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\TooManyRequestsHttpException;

class LoginController extends Controller
{

    const MAX_TOKEN_NUMBER = 10;

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'login'  => ['post'],
                    'logout' => ['post'],
                ],
            ],
        ]);
    }

    public function actionLogin()
    {
        $userData = \Yii::$app->request->post('user');
        if (!isset($userData['login']) || strlen($userData['login']) === 0) {
            throw new BadRequestHttpException('Missing login.');
        }
        if (!isset($userData['password']) || strlen($userData['password']) === 0) {
            throw new BadRequestHttpException('Missing password.');
        }
        $user = User::findByUsername($userData['login']);
        if (!$user || !$user->validatePassword($userData['password'])) {
            throw new NotFoundHttpException('Invalid login or password.');
        }
        if (AuthToken::find()->where(['user_id' => $user->id])->count() > static::MAX_TOKEN_NUMBER) {
            throw new TooManyRequestsHttpException('Too many tokens. Try again after 5 minutes.');
        }
        $token = AuthToken::generate($user);

        return [
            'success' => true,
            'error'  => null,
            'token'  => (string)$token->token,
        ];
    }

    public function actionLogout()
    {
        $userData = \Yii::$app->request->post('user');
        if (!isset($userData['token']) || strlen($userData['token']) === 0) {
            throw new BadRequestHttpException('Missing token.');
        }
        $token = AuthToken::find()->where(
            'token = :token'
            , [
            ':token' => $userData['token'],
        ])->one();
        if (!$token) {
            throw new NotFoundHttpException('Can\'t find token.');
        }

        return [
            'success' => (bool)$token->delete(),
            'error'  => null,
        ];
    }
}
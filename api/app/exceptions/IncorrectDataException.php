<?php
/**
 * Created by PhpStorm.
 * User: Ralt
 * Date: 17.11.2016
 * Time: 21:27
 */

namespace app\exceptions;

use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class IncorrectDataException extends BadRequestHttpException
{
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct(404, $message, $code, $previous);
    }
}
class User {
    static login(event) {
        let request = {
            url: 'api/login/login',
            data: {
                    user:{ 
                        login: document.getElementById('login').value,
                        password: document.getElementById('pass').value 
                  } },
            method: 'POST'
        };
        
        loader.onStart(event.target);
        setRequest(request,function(xhr) {
            if (xhr.status == 200) {
                localStorage.setItem("User[token]", JSON.parse(xhr.responseText).token);
            }
            loader.onFinish(event.target);
        });
    }
    
    static logout(event) {
        let request = {
            url: 'api/login/logout',
            data: {
                    user:{ 
                        token: localStorage.getItem(localStorage.getItem("currentLogin")) 
                  } },
            method: 'POST'
        }
        
        loader.onStart(event.target);
        setRequest(request, function(xhr) {
            if (xhr.status == 200) {    
                localStorage.removeItem("User[token]");
            }
            loader.onFinish(event.target);
        });
    }
}
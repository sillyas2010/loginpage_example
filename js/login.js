/**
 * Login user by login and password.
 *
 * Url: /api/login/login
 *
 * Request: {"user": {"login":"admin","password":"admin"}}
 * Response: {"success":true,"error":null,"token":"wCzw1gAfduq0g1tqb8jOdWiEsSxnVsk_"}
 * Error: {"name":"Not Found","message":"Invalid login or password.","code":0,"status":404,"type":"yii\\web\\NotFoundHttpException"}
 */


/**
 * Logout user by token.
 *
 * Url: /api/login/logout
 *
 * Request: {"user": {"token":"wCzw1gAfduq0g1tqb8jOdWiEsSxnVsk_"}}
 * Response: {"success":true,"error":null}
 * Error: {"name":"Not Found","message":"Invalid token.","code":0,"status":404,"type":"yii\\web\\NotFoundHttpException"}
 */

var btn_login = document.getElementById('log-in');
var btn_logout = document.getElementById('log-out');

btn_login.addEventListener('click', User.login);

/*btn_logout.addEventListener('click', User.logout);*/


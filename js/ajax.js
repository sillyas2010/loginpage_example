function setRequest(request, callback) {
    var xhr = new XMLHttpRequest();
    
    xhr.open(request.method, request.url, true);
    
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.timeout = 30000;

    xhr.ontimeout = function() {
        switchNotify(xhr);
    };
    
    xhr.send(JSON.stringify(request.data));
    
    xhr.onreadystatechange = function() {
        while (xhr.readyState != 4){
            return;
        }    
        
        switchNotify(xhr);    
            
        callback(xhr);
                
    };   
};
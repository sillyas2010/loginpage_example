
var loader = {
    element: "",
    loadingContainer: "",
    onStart: function(element){
        element.classList.add("disabled");
        element.disabled = true;
        loadingContainer = document.createElement('div');
        loadingContainer.classList.add("overlay");
        loadingContainer.innerHTML = "<i class='fa fa-refresh fa-spin'></i>";
        loader.loadingContainer = loadingContainer;
        element.appendChild(loadingContainer);
    },
    onFinish: function(element){
        element.removeChild(loader.loadingContainer);
        element.disabled = false;
        element.classList.remove("disabled");
    }
};

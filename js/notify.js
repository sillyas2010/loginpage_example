
function switchNotify(xhr) {
    if(xhr.status == 200) {
       var message = "Success";
       var type = "success";
    }
    else {
       var message = JSON.parse(xhr.responseText).message;
       var type = "danger";
    }
    
    setNotify(message,type);
};

function setNotify(message,type) {
    $.notify({
        message: message
    },{
        element: 'body',
        type: type
    });
};